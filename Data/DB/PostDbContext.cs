using Microsoft.EntityFrameworkCore;
using WebApplication4.Models.Entities.CommentEntity;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.Entities.User;

namespace WebApplication4.Data.DB
{
    public class PostDbContext : DbContext
    {
        public PostDbContext(DbContextOptions options)
            : base(options)
        {
        }
        
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Like> Likes{ get; set; }
        public DbSet<Comment> Comments { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
//            modelBuilder.Entity<Post>().ToTable("PostEntities");
//            modelBuilder.Entity<User>().ToTable("UserEntities");
//            modelBuilder.Entity<Like>().ToTable("LikeEntities");
            modelBuilder.Entity<User>()
                .HasMany(u => u.Posts)
                .WithOne(p => p.User)
                .HasForeignKey(p => p.UserId);
            modelBuilder.Entity<Post>()
                .HasMany(p => p.Likes)
                .WithOne(l => l.Post)
                .HasForeignKey(l => l.PostId);
            modelBuilder.Entity<Post>()
                .HasMany(p => p.Comments)
                .WithOne(c => c.Post)
                .HasForeignKey(c => c.PostId);
        }
    }
    
}