using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Remotion.Linq.Clauses;
using WebApplication4.Data.DB;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.Like;
using WebApplication4.Models.RepoResults.Like;

namespace WebApplication4.Repository
{
    public interface ILikeRepository
    {
        Task<CreateLikeRepoResult> CreateLikeAsync(CreateLikeRepoParam createLikeRepoParam);
        Task DeleteLikeAsync(string id);
        Task<CreateLikeRepoResult> GetOneLikeAsync(string id);
        Task<List<CreateLikeRepoResult>> GetAllLikesAsync(string postId);
    }
    public class LikeRepository:ILikeRepository
    {
        private readonly ILogger<ILikeRepository> _logger;
        private readonly PostDbContext _context;

        public LikeRepository(ILogger<ILikeRepository> logger, PostDbContext context)
        {
            _logger = logger;
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateLikeRepoResult> CreateLikeAsync(CreateLikeRepoParam createLikeRepoParam)
        {
            if (createLikeRepoParam == null)
            {
                _logger.LogError("CreateLikeRepoParam is null,CreateLikeAsync LikeRepository");
                throw new Exception("CreateLikeRepoParam is null, in CreateLikeAsync in LikeRepository");

            }

            if (!Guid.TryParse(createLikeRepoParam.PostId, out var postId))
            {
                _logger.LogError("guid(postID) didnt pars, CreateCommentAsync CommentRepository");
                throw new Exception("Guid(postId) didn't Parse ");
            }

            if (!Guid.TryParse(createLikeRepoParam.UserId, out var userId))
            {
                _logger.LogError("guid(userID) didnt pars, CreateCommentAsync CommentRepository");
                throw new Exception("Guid(userID) didn't Parse ");
            }

            var like = new Like
            {
                PostId = postId,
                UserId = userId
            };

            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == like.PostId);

            post.LikeCount++;
            _context.Posts.Update(post);
            
            var createdLike = await _context.Likes.AddAsync(like);
            await _context.SaveChangesAsync();

            return new CreateLikeRepoResult(createdLike.Entity);

        }
        

        
        public async Task<CreateLikeRepoResult> GetOneLikeAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null, GetOneLikeAsync LikeRepository");
                throw new Exception("id is null");
            }
            var like = await _context.Likes.FirstOrDefaultAsync(like1 =>like1.Id.ToString()==id );
            
            if (like != null) return new CreateLikeRepoResult(like);
            _logger.LogError("like is not in DB with this id,GetOneLikeAsync LikeRepository");
            throw new Exception("there was no 'Like' matching this id ");

        }
        
        public async Task<List<CreateLikeRepoResult>> GetAllLikesAsync(string postId)
        {
            if (postId==null)
            {
                _logger.LogError("postId is null, GetAllLikesAsync LikeRepository");
                throw new Exception("postID is null");
            }
            var allLikes = await _context.Likes
                .Where(c => c.PostId.ToString() == postId)
                .ToListAsync();
            
            if (allLikes == null)
            {
                _logger.LogError("no like for this post");
                throw new Exception("there is no 'Like' in database for this post");
            }
            var postLikes= new List<CreateLikeRepoResult>();
            foreach (var like in allLikes)
            {
                postLikes.Add(new CreateLikeRepoResult(like));
            }

            return postLikes;
        }
        
        public async Task DeleteLikeAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null, DeleteLikeAsync LikeRepository");
                throw new Exception("id is null");
            }

            var like = await _context.Likes.FirstOrDefaultAsync(lik => lik.Id.ToString() == id);
            try
            {
                _context.Likes.Remove(like);
                var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == like.PostId);
                post.LikeCount--;
                _context.Posts.Update(post);
                
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                _logger.LogError("like not removed,DeleteLikeAsync LikeRepository");
                throw new Exception("like didnt remove");
            }
            
        }
    }
}