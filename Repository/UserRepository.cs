using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplication4.Data.DB;
using WebApplication4.Models.Entities.User;
using WebApplication4.Models.RepoParams.User;
using WebApplication4.Models.RepoResults.User;

namespace WebApplication4.Repository
{
    public interface IUserRepository
    {
        Task<CreateUserRepoResult> CreateUserAsync(CreateUserRepoParam createUserRepoParam);
        Task<CreateUserRepoResult> UpdateUserAsync(UpdateUserRepoParam updateUserRepoParam,string id);
        Task<CreateUserRepoResult> GetOneUserAsync(string id);
        Task<List<CreateUserRepoResult>> GetAllUsersAsync();
        Task DeleteUserAsync(string id);


    }
    
    public class UserRepository:IUserRepository
    {
        private readonly ILogger<IUserRepository> _logger;
        private readonly PostDbContext _context;
        
        public UserRepository(ILogger<IUserRepository> logger, PostDbContext context)
        {
            _logger = logger;
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<CreateUserRepoResult> CreateUserAsync(CreateUserRepoParam createUserRepoParam)
        {
            if (createUserRepoParam == null)
            {
                _logger.LogError("CreateUserRepoParam is null, CreateUserAsync UserRepository");
                throw new Exception("CreateUserRepoParam is null");
                
            }

            var userEntity = new User
            {
                FirstName = createUserRepoParam.FirstName,
                LastName = createUserRepoParam.LastName,
                UserName = createUserRepoParam.UserName,
                Password = createUserRepoParam.Password,
            };

            var createdUser = await _context.Users.AddAsync(userEntity);
            await _context.SaveChangesAsync();
            
            return new CreateUserRepoResult(createdUser.Entity);
            
        }
        
        public async Task<CreateUserRepoResult> UpdateUserAsync(UpdateUserRepoParam updateUserRepoParam,string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,UpdateUserAsync UserRepository");
                throw new Exception("Id is null");
            }
            
            if (updateUserRepoParam == null)
            {
                _logger.LogError("UpdateUserRepoParam is null,UpdateUserAsync UserRepository");
                throw new Exception("UpdateUserRepoParam null");

            }

            var user = await _context.Users.FirstOrDefaultAsync(user1 =>user1.Id.ToString()==id);
            if (user == null)
            {
                _logger.LogError("user is not in DB,UpdateUserAsync UserRepository");
                throw new Exception("this User is not in DB to be Updated");

            }
            
            if ( !Guid.TryParse(id, out var userId))
            {
                _logger.LogError("guid(id) didnt pars, UpdateUserAsync UserRepository");
                throw new Exception("Guid(id) didn't Parse ");
            }

            var updatedUser=new User
            {
                Id = userId,
                FirstName = updateUserRepoParam.FirstName,
                LastName = updateUserRepoParam.LastName,
                UserName = updateUserRepoParam.UserName,
                Password = updateUserRepoParam.Password
            };
            
            _context.Entry(updatedUser).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            

            return new CreateUserRepoResult(updatedUser);
        }
        
        
        public async Task<CreateUserRepoResult> GetOneUserAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,GetOneUserAsync UserRepository");
                throw new Exception("Id is null");
            }
            
            var user = await _context.Users.FirstOrDefaultAsync(user1 => user1.Id.ToString()==id);

            if (user != null) return new CreateUserRepoResult(user);
            _logger.LogError("user is not in DB with this id,GetOneUserAsync UserRepository");
            throw new Exception("there was no user matching this id ");

        }
        
        public async Task<List<CreateUserRepoResult>> GetAllUsersAsync()
        {
            var allUsers = await _context.Users.ToListAsync();

            if (allUsers == null)
            {
                _logger.LogError("No User in DB,GetAllUsersAsync UserRepository");
                throw new Exception("there is no User in database");

            }
            var users= new List<CreateUserRepoResult>();
            foreach (var user in allUsers)
            {
                users.Add(new CreateUserRepoResult(user));
            }

            return users;
        }
        
        public async Task DeleteUserAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,DeleteUserAsync UserRepository");
                throw new Exception("Id is null");
            }
            try
            {
                _context.Users.Remove(await _context.Users.FirstOrDefaultAsync(user =>user.Id.ToString()==id ));
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                _logger.LogError("user not removed,DeleteUserAsync UserRepository");
                throw new Exception("user didnt remove");
            }

            
        }
    }
}