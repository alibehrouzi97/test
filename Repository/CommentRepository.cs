using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplication4.Data.DB;
using WebApplication4.Models.Entities.CommentEntity;

using WebApplication4.Models.RepoParams.Comment;
using WebApplication4.Models.RepoResults.Comment;


namespace WebApplication4.Repository
{
    public interface ICommentRepository
    {
        Task<CreateCommentRepoResult> CreateCommentAsync(CreateCommentRepoParam commentRepoParam);
        Task<CreateCommentRepoResult> UpdateCommentAsync(UpdateCommentRepoParam updateCommentRepoParam,string id);
        Task<CreateCommentRepoResult> GetOneCommentAsync(string id);
        Task DeleteCommentAsync(string id);
        Task<List<CreateCommentRepoResult>> GetAllCommentsAsync(string postId,DateTime? day,DateTime? end,DateTime? start);

    }
    public class CommentRepository:ICommentRepository
    {
        private readonly ILogger<ICommentRepository> _logger;
        private readonly PostDbContext _context;

        public CommentRepository(ILogger<ICommentRepository> logger, PostDbContext context)
        {
            _logger = logger;
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public async Task<CreateCommentRepoResult> CreateCommentAsync(CreateCommentRepoParam commentRepoParam)
        {
            if (commentRepoParam == null)
            {
                _logger.LogError("CreateCommentRepoParam is  null,CreateCommentAsync in CommentRepository");
                throw new Exception("CreateCommentRepoParam is  null, in CreateCommentAsync in CommentRepository");

            }


            if (Guid.TryParse(commentRepoParam.PostId, out var postId))
            {
            }
            else
            {
                _logger.LogError("guid(postID) didnt pars, CreateCommentAsync CommentRepository");
                throw new Exception("Guid(postId) didn't Parse ");
            }

            if ( Guid.TryParse(commentRepoParam.UserId, out var userId))
            {
            }
            else
            {
                _logger.LogError("guid(userID) didnt pars, CreateCommentAsync CommentRepository");
                throw new Exception("Guid(userID) didn't Parse ");
            }
           
            
            var commentEntity = new Comment
            {
                CreateDate = DateTime.Now,
                PostId = postId,
                Description = commentRepoParam.Description,
                UserId =userId
            };
            
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == commentEntity.PostId);

            post.CommentCount++;
            _context.Posts.Update(post);
            
            var createdComment = await _context.Comments.AddAsync(commentEntity);
            await _context.SaveChangesAsync();

            return new CreateCommentRepoResult(createdComment.Entity);
        }
        
        public async Task<CreateCommentRepoResult> UpdateCommentAsync(UpdateCommentRepoParam updateCommentRepoParam,string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,UpdateCommentAsync CommentRepository");
                throw new Exception("Id is null");
            } 
            if (updateCommentRepoParam == null)
            {
                _logger.LogError("UpdateCommentRepoParam is  null, updateCommentAsync in CommentManager");
                throw new Exception("UpdateCommentRepoParam is  null, in updateCommentAsync in CommentManager");

            }
            
            var comment = await _context.Comments.FirstOrDefaultAsync(c =>c.Id.ToString()==id);
            if (comment == null)
            {
                _logger.LogError("comment not in DB with this id,UpdateCommentAsync CommentRepository");
                throw new Exception("this Comment is not in DB to be Updated");

            }
            

            if (!Guid.TryParse(updateCommentRepoParam.PostId, out var postId))
            {
                _logger.LogError("guid(postID) didnt pars, UpdateCommentAsync CommentRepository");
                throw new Exception("Guid(postId) didn't Parse ");
            }

            if ( !Guid.TryParse(updateCommentRepoParam.UserId, out var userId))
            {
                _logger.LogError("guid(userID) didnt pars, UpdateCommentAsync CommentRepository");
                throw new Exception("Guid(userID) didn't Parse ");
            }
            if ( !Guid.TryParse(id, out var commentId))
            {
                _logger.LogError("guid(id) didnt pars, UpdateCommentAsync CommentRepository");
                throw new Exception("Guid(id) didn't Parse ");
            }

            var updatedComment=new Comment
            {
                CreateDate = comment.CreateDate,
                Id = commentId,
                Description = updateCommentRepoParam.Description,
                PostId = postId,
                UserId = userId
            };
            
            _context.Entry(updatedComment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            

            return new CreateCommentRepoResult(updatedComment);
        }
        
        public async Task<CreateCommentRepoResult> GetOneCommentAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,GetOneCommentAsync CommentRepository");
                throw new Exception("Id is null");
            } 
            var comment = await _context.Comments.FirstOrDefaultAsync(c =>c.Id.ToString()==id );
            if (comment == null)
            {
                _logger.LogError("no comment in DB with this id,GetOneCommentAsync CommentRepository");
                throw new Exception("there was no 'comment' matching this id ");

            }

            return new CreateCommentRepoResult(comment);

        }
        
        public async Task<List<CreateCommentRepoResult>> GetAllCommentsAsync(string postId,DateTime? day,DateTime? end,DateTime? start)
        {

            
            
            if (postId==null)
            {
                _logger.LogError("postId is null, GetAllCommentsAsync CommentRepository");
                throw new Exception("postID is null");
            }

            IQueryable<Comment> cm = _context.Comments;
                
            
//            var comments = from c in _context.CommentEntities
//                select c;
//            comments = comments.Where(p => p.PostId.Equals(postId));
           // var allComments = await  comments.ToListAsync();
           
           cm=cm.Where(c => c.PostId.ToString() == postId);
           if (day != null)
           {
               cm = cm.Where(c => c.CreateDate.Day == DateTime.Parse(day.ToString()).Day);
           }

           if (end != null)
           {
               cm = cm.Where(p => p.CreateDate < end);
           }
           if (start != null)
           {
               cm = cm.Where(p => p.CreateDate > start);
           }

           var allComments = await cm.ToListAsync();
           
            if (allComments == null)
            {
                _logger.LogError("no comment for this post");
                throw new Exception("there is no 'Comment' in database for this post");

            }
            var postComments= new List<CreateCommentRepoResult>();
            foreach (var comment in allComments)
            {
                postComments.Add(new CreateCommentRepoResult(comment));
            }

            
            
            return postComments;
        }
        
        public async Task DeleteCommentAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,DeleteCommentAsync CommentRepository");
                throw new Exception("Id is null");
            }

            var comment = await _context.Comments.FirstOrDefaultAsync(cm => cm.Id.ToString() == id);
            
            try
            {
                _context.Comments.Remove(comment);
                
                var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == comment.PostId);
                post.CommentCount--;
                _context.Posts.Update(post);
                
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                _logger.LogError("comment not removed,DeleteCommentAsync CommentRepository");
                throw new Exception("comment didnt remove");
            }
            
        }
    }
}