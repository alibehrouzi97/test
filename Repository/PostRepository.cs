using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using WebApplication4.Data.DB;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.Post;
using WebApplication4.Models.RepoResults.Post;

namespace WebApplication4.Repository
{

    public interface IPostRepository
    {
        Task<CreatePostRepoResult> CreatePostAsync(CreatePostRepoParam createPostRepoParam);
        Task<List<CreatePostRepoResult>> GetAllPostsAsync(DateTime? day,DateTime? end,DateTime? start);
        Task<CreatePostRepoResult> GetOnePostAsync(string id);
        Task<CreatePostRepoResult> UpdatePostAsync(UpdatePostRepoParams updatePostRepoParams,string id);
        Task DeletePostAsync(string id);

    }
    public class PostRepository : IPostRepository
    {
        private readonly ILogger<IPostRepository> _logger;
        private readonly PostDbContext _context;

        public PostRepository(ILogger<IPostRepository> logger, PostDbContext context)
        {
            _logger = logger;
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

        }
        public async Task<CreatePostRepoResult> CreatePostAsync(CreatePostRepoParam createPostRepoParam)
        {
            if (createPostRepoParam == null)
            {
                _logger.LogError("CreatePostRepoParam");
                throw new Exception("CreatePostRepoParam is null"); 
            }
            
            if ( !Guid.TryParse(createPostRepoParam.UserId, out var userId))
            {
                _logger.LogError("guid(userID) didnt pars, createPostAsync PostRepository");
                throw new Exception("Guid(userID) didn't Parse ");
            }

            
            var postEntity = new Post
            {
                UserId = userId,
                Title = createPostRepoParam.Title,
                Description = createPostRepoParam.Description,
                EditDate = null,
                CreateDate = DateTime.Now,
                LikeCount = 0,
                CommentCount = 0
            };
            
            var createdPost = await _context.Posts.AddAsync(postEntity);
            await _context.SaveChangesAsync();

          
            return new CreatePostRepoResult(createdPost.Entity);

        }

        
        public async Task<CreatePostRepoResult> UpdatePostAsync(UpdatePostRepoParams updatePostRepoParams,string id)
        {
            
            
            if (updatePostRepoParams == null)
            {
                _logger.LogError("UpdatePostRepoParams is null");
                throw new Exception("Update Repo Param null");

            }
          
            
            
        
          
            var post = await _context.Posts.FirstOrDefaultAsync(post1 =>post1.Id.ToString()==id);
            if (post == null)
            {
                _logger.LogError("Post not found in DB , UpdatePostAsync");
                throw new Exception("this post is not in DB to be Updated");
            }
            

            if ( !Guid.TryParse(updatePostRepoParams.UserId, out var userId))
            {
                _logger.LogError("guid(userID) didnt pars, UpdatePostAsync PostRepository");
                throw new Exception("Guid(userID) didn't Parse ");
            }
            if ( !Guid.TryParse(id, out var postId))
            {
                _logger.LogError("guid(id) didnt pars, UpdatePostAsync PostRepository");
                throw new Exception("Guid(id) didn't Parse ");
            }

            var updatedPost = new Post
            {
                UserId = userId,
                Id = postId,
                Title = updatePostRepoParams.Title,
                Description = updatePostRepoParams.Description,
                CreateDate = post.CreateDate,
                EditDate = DateTime.Now,
                LikeCount = post.LikeCount,
                CommentCount = post.CommentCount
            };
            
            //_context.Entry(updatedPost).State = EntityState.Modified;
            _context.Posts.Update(updatedPost);
            
            await _context.SaveChangesAsync();
            
            return new CreatePostRepoResult(updatedPost);
        }


        public async Task<CreatePostRepoResult> GetOnePostAsync(string id)
        {
            
            if (id==null)
            {
                _logger.LogError("id is null,GetOnePostAsync");
                throw new Exception("Id is null");
            }
            
            var post = await _context.Posts.FirstOrDefaultAsync(post1 =>post1.Id.ToString()==id );
            
            if (post != null) return new CreatePostRepoResult(post);
            _logger.LogError("post not found in DB, GetOnePostAsync");
            throw new Exception("there was no post matching this id ");

        }
        
        public async Task<List<CreatePostRepoResult>> GetAllPostsAsync(DateTime? day,DateTime? end,DateTime? start)
        {

            IQueryable<Post> ps = _context.Posts;
            
            if (day != null)
            {
                ps = ps.Where(p => p.CreateDate.Day == DateTime.Parse(day.ToString()).Day);
            }

            if (end != null)
            {
                ps = ps.Where(p => p.CreateDate < end);
            }
            if (start != null)
            {
                ps = ps.Where(p => p.CreateDate > start);
            }

            var allPosts = await ps.ToListAsync();
            
            if (allPosts == null)
            {
                _logger.LogError("no post in DB,GetAllPostsAsync");
                throw new Exception("there is no post in database");

            }
            
            
            
            var posts= new List<CreatePostRepoResult>();
            foreach (var post in allPosts)
            {
                posts.Add(new CreatePostRepoResult(post));
            }

            return posts;
        }
        
        public async Task DeletePostAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,DeletePostAsync");
                throw new Exception("Id is null");
            }

            try
            {
                _context.Posts.Remove(await _context.Posts.FirstOrDefaultAsync(post1 =>post1.Id.ToString()==id ));
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                _logger.LogError("post not removed,DeletePostAsync PostRepository");
                throw new Exception("post didnt remove");
            }


        }


    }
}