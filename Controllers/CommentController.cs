using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication4.Manager;
using WebApplication4.Models.ApiModels.CommentApiModels;
using WebApplication4.Models.ApiModels.LikeApiModels;

namespace WebApplication4.Controllers
{
    [Route("comment")]
    public class CommentController:ControllerBase
    {
        private readonly ILogger<CommentController> _logger;
        private readonly ICommentManager _commentManager;
        private readonly IPostManager _postManager;

        public CommentController(ILogger<CommentController> logger, ICommentManager commentManager,
            IPostManager postManager)
        {
            _logger = logger;
            _commentManager = commentManager;
            _postManager = postManager;
        }
        
        //Post
        [HttpPost]
        public async Task<IActionResult> CreateCommentAsync([FromBody] CreateCommentApiModel commentModel)
        {
            if (commentModel == null)
            {
                _logger.LogError("CreateCommentApiModel is null in CreateCommentAsync in CommentController");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    commentModel
                });
            }

            try
            {
                var commentObject = await _commentManager.CreateCommentAsync(commentModel);

                return Ok(new
                {
                    Status = "OK",
                    Comment = commentObject.GetComplete()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound("comment not created");
            }

        }
        
        //Get Comment List
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAllCommentsAsync([FromRoute] string id,[FromQuery] DateTime? day,[FromQuery] DateTime? end,[FromQuery] DateTime? start)
        {
            if (id==null)
            {
                _logger.LogError("id is null,GetAllPostsAsync CommentController");
                throw new Exception("id is null");
            }
            
            try
            {
                await _postManager.GetOnePostAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }


            try
            {
                var commentObjects = await _commentManager.GetAllCommentsAsync(id,day,end,start);
            
                var commentSummary = new List<CommentGetSummaryApiModels>();
                foreach (var s in commentObjects)
                {
                    commentSummary.Add(s.GetSummary());
                }

                return Ok(commentSummary);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }

        }
        
        //put
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCommentAsync([FromBody] UpdateCommentApiModel commentModel,[FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,UpdateCommentAsync CommentController");
                throw new Exception("id is null");
            }
            if (commentModel == null)
            {
                _logger.LogError("UpdateCommentApiModel is null in UpdatePostAsync in CommentController");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    commentModel
                });
            }

            try
            {
                var commentObject = await _commentManager.UpdateCommentAsync(commentModel,id);
                return Ok(new
                {
                    Status = "Ok",
                    Comment = commentObject.GetComplete()
                });
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message);
               return NotFound("comment didnt update");
            }


            
        }
        
        //Get One Comment

      
        [HttpGet("{postTd}/{id}")]
        public async Task<IActionResult> GetOneCommentAsync([FromRoute] string postId,[FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,GetOnePostAsync CommentController");
                throw new Exception("id is null");
            }
            if (postId==null)
            {
                _logger.LogError("id is null,GetOnePostAsync CommentController");
                throw new Exception("id is null");
            }
            try
            {
                await _postManager.GetOnePostAsync(postId);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }

            try
            {
                var commentObjects = await _commentManager.GetAllCommentsAsync(postId,null,null,null);

                var cm= new CommentGetCompleteApiModels();
                foreach (var c in commentObjects)
                {
                    if (c.GetComplete().Id==id)
                    {
                        cm = c.GetComplete();
                    
                    }
                }

                if (cm.Id==null)
                {
                    _logger.LogError("no comment with this id for this post,GetOnePostAsync CommentController");
                    return NotFound("there is no comment with this id for this post");
                }

                return Ok(cm);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
        }

        //Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCommentAsync([FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,DeletePostAsync CommentController");
                throw new Exception("id is null");
            }
            try
            {
                try
                {
                    await _commentManager.GetOneCommentAsync(id);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return NotFound(e.Message);
                }
                
                await _commentManager.DeleteCommentAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            return Ok("'comment' Have Been Deleted");            
        }
        
    }
}