using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication4.Manager;
using WebApplication4.Models.ApiModels.PostApiModels;
using WebApplication4.Models.Entities.PostEntity;

namespace WebApplication4.Controllers
{
    [Route("post")]
    public class PostController : ControllerBase
    {
        private readonly ILogger<PostController> _logger;
        private readonly IPostManager _postManager;

        public PostController(ILogger<PostController> logger, IPostManager postManager)
        {
            _logger = logger;
            _postManager = postManager;
        }
        
        //Post
        [HttpPost]
        public async Task<IActionResult> CreatePostAsync([FromBody] CreatePostApiModel postModel)
        {
            if (postModel == null)
            {
                _logger.LogError("CreatePostApiModel is null in CreateCommentAsync in Controller");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    postModel
                });
            }

            try
            {
                var postObject = await _postManager.CreatePostAsync(postModel);

                return Ok(new
                {
                    Status = "OK",
                    Post = postObject.GetComplete()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound("post not created");
            }

        }
        
        //put
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePostAsync([FromBody] UpdatePostApiModel postModel,[FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("ID is null ,DeletePostAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }
            if (postModel == null)
            {
                _logger.LogError("UpdatePostApiModel is null in UpdatePostAsync in Controller");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    postModel
                });
            }

            try
            {
                var postObject = await _postManager.UpdatePostAsync(postModel,id);
                return Ok(new
                {
                    Status = "Ok",
                    Post = postObject.GetComplete()
                });

            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound("post not updated");
            }


        }
        
        //Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePostAsync([FromRoute] string id)
        {
            
            if (id==null)
            {
                _logger.LogError("ID is null ,DeletePostAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }

            try
            {
                try
                {
                    await _postManager.GetOnePostAsync(id);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return NotFound(e.Message);
                }
                
                await _postManager.DeletePostAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            return Ok("Post Have Been Deleted");            
        }
        
        
        
        //Get Post List
        
        [HttpGet]
        public async Task<IActionResult> GetAllPostsAsync([FromQuery] DateTime? day,[FromQuery] DateTime? end,[FromQuery] DateTime? start)
        {
            
            try
            {
                var postObjects = await _postManager.GetAllPostsAsync(day,end,start);
                var postsSummary = new List<PostGetSummaryApiModels>();
                foreach (var s in postObjects)
                {
                    postsSummary.Add(s.GetSummary());
                }

                return Ok(postsSummary);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
        }
        
        
        
        
        //Get One Post
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOnePostAsync([FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("ID is null ,GetOnePostAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }
            try
            {
                var postObject = await _postManager.GetOnePostAsync(id);
                return Ok(postObject.GetComplete());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
        }
    }
}