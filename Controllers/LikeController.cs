using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication4.Manager;
using WebApplication4.Models.ApiModels.LikeApiModels;
using WebApplication4.Models.ApiModels.PostApiModels;

namespace WebApplication4.Controllers
{
    [Route("like")]
    public class LikeController : ControllerBase
    {
        private readonly ILogger<LikeController> _logger;
        private readonly ILikeManager _likeManager;
        private readonly IPostManager _postManager;

        public LikeController(ILogger<LikeController> logger, ILikeManager likeManager,IPostManager postManager)
        {
            _logger = logger;
            _likeManager = likeManager;
            _postManager = postManager;
        }
        
        //Post
        [HttpPost]
        public async Task<IActionResult> CreateLikeAsync( [FromBody] CreateLikeApiModel likeModel)
        {
            if (likeModel == null)
            {
                _logger.LogError("CreateLikeApiModel is null in CreateCommentAsync in LikeController");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    likeModel
                });
            }

            try
            {

                var likeObject = await _likeManager.CreateLikeAsync(likeModel);

                return Ok(new
                {
                    Status = "OK",
                    Like = likeObject.GetComplete()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }

        }
        
        
        //Get Like List
        
        [HttpGet("{postId}")]
        public async Task<IActionResult> GetAllLikesAsync([FromRoute] string postId)
        {
            if (postId==null)
            {
                _logger.LogError("postId is null,GetAllPostsAsync LikeController");
                throw new Exception("postId is null");
            }

            try
            {
                await _postManager.GetOnePostAsync(postId);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }


            try
            {
                var likeObjects = await _likeManager.GetAllLikesAsync(postId);
            
                var likesSummary = new List<LikeGetSummaryApiModels>();
                foreach (var s in likeObjects)
                {
                    likesSummary.Add(s.GetSummary());
                }

                return Ok(likesSummary);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }

        }
        
        //Get One Like
//        
//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetOnePostAsync(Guid id)
//        {
//            var likeObject = await _likeManager.GetOneLikeAsync(id);
//
//            if (likeObject == null)
//            {
//                return NotFound("there is no 'like' with this id");
//
//            }
//
//            return Ok(likeObject.GetComplete());
//        }
        
        //Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLikeAsync([FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,DeletePostAsync LikeController");
                throw new Exception("id is null");
            }
            try
            {
                try
                {
                    await _likeManager.GetOneLikeAsync(id);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return NotFound(e.Message);
                }
                
                await _likeManager.DeleteLikeAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            return Ok("'Like' Have Been Deleted");            
        }
    }
}