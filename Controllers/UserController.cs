using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using WebApplication4.Manager;
using WebApplication4.Models.ApiModels.UserApiModels;

namespace WebApplication4.Controllers
{
    [Route("user")]
    public class UserController:ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserManager _userManager;

        public UserController(ILogger<UserController> logger, IUserManager userManager)
        {
            _logger = logger;
            _userManager = userManager;
        }
        
        //Post
        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromBody] CreateUserApiModels userModel)
        {
            if (userModel == null)
            {
                _logger.LogError("CreateUserApiModel is null in CreateCommentAsync in Controller");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    userModel
                });
            }

            try
            {
                var userObject = await _userManager.CreateUserAsync(userModel);
                return Ok(new
                {
                    Status = "OK",
                    User = userObject.GetComplete()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound("user not created");
            }
        }
        
        //put
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UpdateUserApiModels userModel,[FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("ID is null ,UpdateUserAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }
            if (userModel == null)
            {
                _logger.LogError("UpdateUserApiModel is null in UpdatePostAsync in Controller");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "Model is null"
                });
            }

            if (!ModelState.IsValid)
            {
                var validationProblemDetails = new ValidationProblemDetails(ModelState);
                return BadRequest(new
                {
                    status = "Error", 
                    message = "error validating model",
                    errors = validationProblemDetails.Errors,
                    userModel
                });
            }

            try
            {
                var userObject = await _userManager.UpdateUserAsync(userModel,id);
                return Ok(new
                {
                    Status = "Ok",
                    User = userObject.GetComplete()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound("user not updated");
            }


            
        }
        //Get User List
        
        [HttpGet]
        public async Task<IActionResult> GetAllUserAsync()
        {
            try
            {
                var userObjects = await _userManager.GetAllUsersAsync();
            
                var usersSummary = new List<UserGetSummaryApiModels>();
                foreach (var s in userObjects)
                {
                    usersSummary.Add(s.GetSummary());
                }

                return Ok(usersSummary);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
        }
        
        
        //Get One User
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneUserAsync([FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("ID is null ,GetOneUserAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }
            try
            {
                var userObject = await _userManager.GetOneUserAsync(id);
                return Ok(userObject.GetComplete());
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound(e.Message);
            }
        }
        
        
        //Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAsync([FromRoute] string id)
        {
            if (id==null)
            {
                _logger.LogError("ID is null ,DeleteUserAsync");
                return BadRequest(new
                {
                    Status = "Error",
                    Message = "id is null"
                });
                
            }
            try
            {
                try
                {
                    await _userManager.GetOneUserAsync(id);
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                    return NotFound(e.Message);
                }

                await _userManager.DeleteUserAsync(id);
            }
            catch (Exception e) 
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }
            return Ok("User Have Been Deleted");            
        }
    }
    
}