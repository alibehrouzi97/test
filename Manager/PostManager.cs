using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebApplication4.Models.ApiModels.PostApiModels;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.Post;
using WebApplication4.Repository;

namespace WebApplication4.Manager
{
    public interface IPostManager
    {
        Task<PostObject> CreatePostAsync(CreatePostApiModel postModel);
        Task<List<PostObject>> GetAllPostsAsync(DateTime? day,DateTime? end,DateTime? start);
        Task<PostObject> GetOnePostAsync(string id);
        Task<PostObject> UpdatePostAsync(UpdatePostApiModel postModel,string id);

        Task DeletePostAsync(string id);

    }
    
    public class PostManager:IPostManager
    {
        private readonly ILogger<PostManager> _logger;
        private readonly IPostRepository _postRepository;

        public PostManager(IPostRepository postRepository, ILogger<PostManager> logger)
        {
            _logger = logger;
            _postRepository = postRepository;
        }
        
        
        public async Task<PostObject> CreatePostAsync(CreatePostApiModel postModel)
        {
            if (postModel == null)
            {
                _logger.LogError("CreatePostAsync PostManager CreatePostApiModel is null");
                throw new Exception("CreatePostApiModel is null");

            }

            var createPostRepoParam = new CreatePostRepoParam(postModel);

            try
            {
                var createdPostRepoResult = await _postRepository.CreatePostAsync(createPostRepoParam);
                return new PostObject(createdPostRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            

        }

        public async Task<PostObject> UpdatePostAsync(UpdatePostApiModel postModel,string id)
        {
            if (postModel == null)
            {
                _logger.LogError("UpdatePostAsync PostManager UpdatePostApiModel is null");
                throw new Exception("UpdatePostApiModel is null");

            }

            var updatePostRepoParam = new UpdatePostRepoParams(postModel);
            try
            {
                var updatedPostRepoResult = await _postRepository.UpdatePostAsync(updatePostRepoParam,id);

                return new PostObject(updatedPostRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }

        }


        
        public async Task<List<PostObject>> GetAllPostsAsync(DateTime? day,DateTime? end,DateTime? start)
        {
            try
            {
                var postRepoResults = await _postRepository.GetAllPostsAsync(day,end,start);
                var allPostObjects = new List<PostObject>();
                foreach (var postRepoResult in postRepoResults)
                {
                    allPostObjects.Add(new PostObject(postRepoResult));
                }

                return allPostObjects;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            


        }

        public async Task<PostObject> GetOnePostAsync(string id)
        {
            if (id == null)
            {
                _logger.LogError("Id is null,GetOnePostAsync PostManager");
                throw new Exception("ID is null in GetOnePostAsync in PostManager");
            }

            try
            {
                var postRepoResult = await _postRepository.GetOnePostAsync(id);
                return new PostObject(postRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
            
        }
        public async Task DeletePostAsync(string id)
        {
            if (id == null)
            {
                _logger.LogError("Id is null,DeletePstAsync PostManager");
                throw new Exception("ID is null in DeletePostAsync in PostManager");
            }

            try
            {
                await _postRepository.DeletePostAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
            
        }
    }
}