using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebApplication4.Models.ApiModels.LikeApiModels;
using WebApplication4.Models.Entities.LikeEntity;

using WebApplication4.Models.RepoParams.Like;

using WebApplication4.Repository;

namespace WebApplication4.Manager
{
    public interface ILikeManager
    {
        Task<LikeObject> CreateLikeAsync(CreateLikeApiModel likeModel);
        Task<List<LikeObject>> GetAllLikesAsync(string postId);
        Task<LikeObject> GetOneLikeAsync(string id);
        Task DeleteLikeAsync(string id);
    }
    public class LikeManager:ILikeManager
    {
        private readonly ILogger<LikeManager> _logger;
        private readonly ILikeRepository _likeRepository;

        public LikeManager(ILikeRepository likeRepository, ILogger<LikeManager> logger)
        {
            _logger = logger;
            _likeRepository = likeRepository;
        }
        
        public async Task<LikeObject> CreateLikeAsync(CreateLikeApiModel likeModel)
        {
            if (likeModel == null)
            {
                _logger.LogError("CreateLikeAsync in LikeManager ,CreateLikeApiModel is null");
                throw new Exception("CreateLikeApiModel is null");

            }

            var createLikeRepoParam = new CreateLikeRepoParam(likeModel);

            try
            {
                var createdLikeRepoResult = await _likeRepository.CreateLikeAsync(createLikeRepoParam);
            
            
                return new LikeObject(createdLikeRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<List<LikeObject>> GetAllLikesAsync(string postId)
        {
            if (postId==null)
            {
                _logger.LogError("postId is null, GetAllLikesAsync LikeManager");
                throw new Exception("postID is null");
            }

            try
            {
                var likeRepoResults = await _likeRepository.GetAllLikesAsync(postId);
                var allLikeObjects = new List<LikeObject>();
                foreach (var r in likeRepoResults)
                {
                    allLikeObjects.Add(new LikeObject(r));
                }

                return allLikeObjects;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<LikeObject> GetOneLikeAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null, GetOneLikeAsync LikeManager");
                throw new Exception("id is null");
            }

            try
            {
                var likeRepoResult = await _likeRepository.GetOneLikeAsync(id);
                return new LikeObject(likeRepoResult);
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task DeleteLikeAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null, DeleteLikeAsync LikeManager");
                throw new Exception("id is null");
            }

            try
            {
                await _likeRepository.DeleteLikeAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
            
        }
        
    }
}