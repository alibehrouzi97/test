using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebApplication4.Models.ApiModels.UserApiModels;
using WebApplication4.Models.Entities.User;
using WebApplication4.Models.RepoParams.User;
using WebApplication4.Repository;

namespace WebApplication4.Manager
{
    public interface IUserManager
    {
        Task<UserObject> CreateUserAsync(CreateUserApiModels userModel);
        Task<UserObject> UpdateUserAsync(UpdateUserApiModels userModel,string id);
        Task<UserObject> GetOneUserAsync(string id);
        Task<List<UserObject>> GetAllUsersAsync();
        Task DeleteUserAsync(string id);
    }
    public class UserManager:IUserManager
    {
        private readonly ILogger<UserManager> _logger;
        private readonly IUserRepository _userRepository;

        public UserManager(IUserRepository userRepository, ILogger<UserManager> logger)
        {
            _logger = logger;
            _userRepository = userRepository;
        }
        
        public async Task<UserObject> CreateUserAsync(CreateUserApiModels userModel)
        {
            if (userModel == null)
            {
                _logger.LogError("CreateUserAsync in UserManager ,CreateUserApiModel is null");
                throw new Exception("CreateUserApiModel is null");

            }

            var createUserRepoParam = new CreateUserRepoParam(userModel);

            try
            {

                var createdUserRepoResult = await _userRepository.CreateUserAsync(createUserRepoParam);
                return new UserObject(createdUserRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<List<UserObject>> GetAllUsersAsync()
        {
            try
            {
                var usersRepoResults = await _userRepository.GetAllUsersAsync();
                List<UserObject> allUserObjects = new List<UserObject>();
                foreach (var r in usersRepoResults)
                {
                    allUserObjects.Add(new UserObject(r));
                }

                return allUserObjects;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<UserObject> GetOneUserAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,GetOneUserAsync UserManager");
                throw new Exception("id is null");
            }

            try
            {
                var userRepoResult = await _userRepository.GetOneUserAsync(id);
                return new UserObject(userRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }
        
        

        public async Task<UserObject> UpdateUserAsync(UpdateUserApiModels userModel,string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,UpdateUserAsync UserManager");
                throw new Exception("id is null");
            }
            if (userModel == null)
            {
                _logger.LogError("UpdateUserAsync in UserManager ,UpdateUserApiModel is null");
                throw new Exception("UpdateUserApiModel is null");

            }
            var updateUserRepoParam=new UpdateUserRepoParam(userModel);

            try
            {
                var updatedUserRepoResult = await _userRepository.UpdateUserAsync(updateUserRepoParam,id);
                return new UserObject(updatedUserRepoResult);
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message);
                throw;
            }

        }
        
        public async Task DeleteUserAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("Id is null,DeleteUserAsync UserManager");
                throw new Exception("id is null");
            }

            try
            {
                await _userRepository.DeleteUserAsync(id);
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message);
                throw;
            }
        }
    }
}