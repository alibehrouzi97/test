using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebApplication4.Models.ApiModels.CommentApiModels;
using WebApplication4.Models.ApiModels.LikeApiModels;
using WebApplication4.Models.Entities.CommentEntity;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Models.RepoParams.Comment;
using WebApplication4.Models.RepoParams.Like;
using WebApplication4.Repository;

namespace WebApplication4.Manager
{
    public interface ICommentManager
    {
        Task<CommentObject> CreateCommentAsync(CreateCommentApiModel commentModel);
        Task<List<CommentObject>> GetAllCommentsAsync(string postId,DateTime? day,DateTime? end,DateTime? start);
        Task<CommentObject> UpdateCommentAsync(UpdateCommentApiModel commentModel,string id);
        Task<CommentObject> GetOneCommentAsync(string id);
        Task DeleteCommentAsync(string id);

    }
    public class CommentManager:ICommentManager
    {
        private readonly ILogger<CommentManager> _logger;
        private readonly ICommentRepository _commentRepository;

        public CommentManager(ILogger<CommentManager> logger, ICommentRepository commentRepository)
        {
            _logger = logger;
            _commentRepository = commentRepository;
        }
        
        public async Task<CommentObject> CreateCommentAsync(CreateCommentApiModel commentModel)
        {
            if (commentModel == null)
            {
                _logger.LogError("CreateCommentAsync in CommentManager ,CreateCommentApiModel is null");
                throw new Exception("CreateCommentApiModel is null");

            }

            var createCommentRepoParam = new CreateCommentRepoParam(commentModel);

            try
            {
                var createCommentRepoResult = await _commentRepository.CreateCommentAsync(createCommentRepoParam);
                return new CommentObject(createCommentRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }


        }
        
        public async Task<CommentObject> UpdateCommentAsync(UpdateCommentApiModel commentModel ,string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,UpdateCommentAsync CommentManager");
                throw new Exception("id is null");
            }
            if (commentModel == null)
            {
                _logger.LogError("UpdateCommentAsync CommentManager UpdateCommentApiModel is null");
                throw new Exception("UpdateCommentApiModel is null");

            }

            var updateCommentRepoParam = new UpdateCommentRepoParam(commentModel);

            try
            {
                var updateCommentRepoResult = await _commentRepository.UpdateCommentAsync(updateCommentRepoParam,id);

                return new CommentObject(updateCommentRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }

        }
        
        public async Task<List<CommentObject>> GetAllCommentsAsync(string postId,DateTime? day,DateTime? end,DateTime? start)
        {
            if (postId==null)
            {
                _logger.LogError("postId is null,DeleteCommentAsync CommentManager");
                throw new Exception("postId is null");
            }

            try
            {
                var commentsRepoResult = await _commentRepository.GetAllCommentsAsync(postId,day,end,start);
                var allCommentObjects = new List<CommentObject>();
                foreach (var r in commentsRepoResult)
                {
                    allCommentObjects.Add(new CommentObject(r));
                }

                return allCommentObjects;
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message);
                throw;
            }
        }
        
        public async Task<CommentObject> GetOneCommentAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,GetOneCommentAsync CommentRepository");
                throw new Exception("Id is null");
            }

            try
            {
                var commentRepoResult = await _commentRepository.GetOneCommentAsync(id);
                return new CommentObject(commentRepoResult);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
            
        }
        
        public async Task DeleteCommentAsync(string id)
        {
            if (id==null)
            {
                _logger.LogError("id is null,DeleteCommentAsync CommentManager");
                throw new Exception("Id is null");
            }

            try
            {
                await _commentRepository.DeleteCommentAsync(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
            
            
        }
        
        
        
    }
}