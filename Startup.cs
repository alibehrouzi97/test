﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication4.Data.DB;
using WebApplication4.Manager;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Repository;

namespace WebApplication4
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           // var conn = @"Server=localhost;Port=5432;Database=WaterFlowMeter;User Id=postgres;Password=123@Password;";
            services.AddEntityFrameworkNpgsql().AddDbContext<PostDbContext>(
                options =>
                    options.UseNpgsql(Configuration.GetConnectionString("PostContext")));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.AddTransient<IPostManager, PostManager>();
            services.AddTransient<IPostRepository, PostRepository>();
            services.AddTransient<IUserManager, UserManager>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ILikeManager, LikeManager>();
            services.AddTransient<ILikeRepository, LikeRepository>();
            services.AddTransient<ICommentManager, CommentManager>();
            services.AddTransient<ICommentRepository, CommentRepository>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}