﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication4.Migrations
{
    public partial class addLike : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "PostEntities",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LikeEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PostId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LikeEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LikeEntities_PostEntities_PostId",
                        column: x => x.PostId,
                        principalTable: "PostEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LikeEntities_UserEntities_UserId",
                        column: x => x.UserId,
                        principalTable: "UserEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PostEntities_UserId",
                table: "PostEntities",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_LikeEntities_PostId",
                table: "LikeEntities",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_LikeEntities_UserId",
                table: "LikeEntities",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PostEntities_UserEntities_UserId",
                table: "PostEntities",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PostEntities_UserEntities_UserId",
                table: "PostEntities");

            migrationBuilder.DropTable(
                name: "LikeEntities");

            migrationBuilder.DropIndex(
                name: "IX_PostEntities_UserId",
                table: "PostEntities");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "PostEntities");
        }
    }
}
