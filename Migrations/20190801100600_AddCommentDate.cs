﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication4.Migrations
{
    public partial class AddCommentDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentEntities_PostEntities_PostId",
                table: "CommentEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_LikeEntities_PostEntities_PostId",
                table: "LikeEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_LikeEntities_UserEntities_UserId",
                table: "LikeEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_PostEntities_UserEntities_UserId",
                table: "PostEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserEntities",
                table: "UserEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PostEntities",
                table: "PostEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LikeEntities",
                table: "LikeEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CommentEntities",
                table: "CommentEntities");

            migrationBuilder.RenameTable(
                name: "UserEntities",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "PostEntities",
                newName: "Posts");

            migrationBuilder.RenameTable(
                name: "LikeEntities",
                newName: "Likes");

            migrationBuilder.RenameTable(
                name: "CommentEntities",
                newName: "Comments");

            migrationBuilder.RenameIndex(
                name: "IX_PostEntities_UserId",
                table: "Posts",
                newName: "IX_Posts_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_LikeEntities_UserId",
                table: "Likes",
                newName: "IX_Likes_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_LikeEntities_PostId",
                table: "Likes",
                newName: "IX_Likes_PostId");

            migrationBuilder.RenameIndex(
                name: "IX_CommentEntities_PostId",
                table: "Comments",
                newName: "IX_Comments_PostId");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Comments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Posts",
                table: "Posts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Likes",
                table: "Likes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comments",
                table: "Comments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_Posts_PostId",
                table: "Likes",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Likes_Users_UserId",
                table: "Likes",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_UserId",
                table: "Posts",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_Posts_PostId",
                table: "Likes");

            migrationBuilder.DropForeignKey(
                name: "FK_Likes_Users_UserId",
                table: "Likes");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_UserId",
                table: "Posts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Posts",
                table: "Posts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Likes",
                table: "Likes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comments",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "UserEntities");

            migrationBuilder.RenameTable(
                name: "Posts",
                newName: "PostEntities");

            migrationBuilder.RenameTable(
                name: "Likes",
                newName: "LikeEntities");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "CommentEntities");

            migrationBuilder.RenameIndex(
                name: "IX_Posts_UserId",
                table: "PostEntities",
                newName: "IX_PostEntities_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Likes_UserId",
                table: "LikeEntities",
                newName: "IX_LikeEntities_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Likes_PostId",
                table: "LikeEntities",
                newName: "IX_LikeEntities_PostId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_PostId",
                table: "CommentEntities",
                newName: "IX_CommentEntities_PostId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserEntities",
                table: "UserEntities",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PostEntities",
                table: "PostEntities",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LikeEntities",
                table: "LikeEntities",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CommentEntities",
                table: "CommentEntities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CommentEntities_PostEntities_PostId",
                table: "CommentEntities",
                column: "PostId",
                principalTable: "PostEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LikeEntities_PostEntities_PostId",
                table: "LikeEntities",
                column: "PostId",
                principalTable: "PostEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LikeEntities_UserEntities_UserId",
                table: "LikeEntities",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PostEntities_UserEntities_UserId",
                table: "PostEntities",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
