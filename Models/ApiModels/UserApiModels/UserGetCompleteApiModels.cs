using System;
using WebApplication4.Models.RepoResults.User;

namespace WebApplication4.Models.ApiModels.UserApiModels
{
    public class UserGetCompleteApiModels
    {
        public UserGetCompleteApiModels(CreateUserRepoResult createUserRepoResult)
        {
            Id = createUserRepoResult.Id;
            FirstName = createUserRepoResult.FirstName;
            LastName = createUserRepoResult.LastName;
            UserName = createUserRepoResult.UserName;
            Password = createUserRepoResult.Password;
            
        }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}