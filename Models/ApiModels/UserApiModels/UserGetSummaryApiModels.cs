using System;
using WebApplication4.Models.RepoResults.User;

namespace WebApplication4.Models.ApiModels.UserApiModels
{
    public class UserGetSummaryApiModels
    {
        public UserGetSummaryApiModels(CreateUserRepoResult createUserRepoResult)
        {
            Id = createUserRepoResult.Id;
            UserName = createUserRepoResult.UserName;
            
        }
        
            
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}