using System;

namespace WebApplication4.Models.ApiModels.PostApiModels
{
    public class UpdatePostApiModel
    {
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }
}