using System;
using WebApplication4.Models.RepoResults.Post;

namespace WebApplication4.Models.ApiModels.PostApiModels
{
    public class PostGetCompleteApiModels
    {
        public PostGetCompleteApiModels(CreatePostRepoResult _createPostRepoResult)
        {
            Id = _createPostRepoResult.Id;
            Title = _createPostRepoResult.Title;
            Description = _createPostRepoResult.Description;
            CreateDate = _createPostRepoResult.CreateDate;
            EditDate = _createPostRepoResult.EditDate;
            LikeCount = _createPostRepoResult.LikeCount;
            UserId = _createPostRepoResult.UserId;
            CommentCount = _createPostRepoResult.CommentCount;
        }


        public int CommentCount { get; set; }
        public string UserId { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public int LikeCount { get; set; }  
    }
}