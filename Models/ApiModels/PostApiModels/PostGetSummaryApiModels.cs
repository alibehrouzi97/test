using System;
using WebApplication4.Models.RepoResults.Post;

namespace WebApplication4.Models.ApiModels.PostApiModels
{
    public class PostGetSummaryApiModels
    {
        
        public PostGetSummaryApiModels(CreatePostRepoResult _createPostRepoResult)
        {
            Id = _createPostRepoResult.Id;
            Title = _createPostRepoResult.Title;
            CreateDate = _createPostRepoResult.CreateDate;
            LikeCount = _createPostRepoResult.LikeCount;
            UserId = _createPostRepoResult.UserId;
            CommentCount = _createPostRepoResult.CommentCount;
        }
        public int CommentCount { get; set; }
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }

        public DateTime CreateDate { get; set; }

        public int LikeCount { get; set; }          
    }
}