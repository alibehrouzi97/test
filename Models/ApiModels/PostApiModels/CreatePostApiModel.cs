using System;

namespace WebApplication4.Models.ApiModels.PostApiModels
{
    public class CreatePostApiModel
    {
        public int CommentCount { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}