using System;

namespace WebApplication4.Models.ApiModels.CommentApiModels
{
    public class UpdateCommentApiModel
    {
        public string PostId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}