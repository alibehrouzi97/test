using System;
using WebApplication4.Models.Entities.PostEntity;

namespace WebApplication4.Models.ApiModels.CommentApiModels
{
    public class CreateCommentApiModel
    {
        public string PostId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}