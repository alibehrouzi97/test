using System;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoResults.Comment;

namespace WebApplication4.Models.ApiModels.CommentApiModels
{
    public class CommentGetSummaryApiModels
    {
        public CommentGetSummaryApiModels(CreateCommentRepoResult commentRepoResult)
        {
            Id = commentRepoResult.Id;
            PostId = commentRepoResult.PostId;
            Description = commentRepoResult.Description;
            UserId = commentRepoResult.UserId;
            CreateDate = commentRepoResult.CreateDate;
        }
        public DateTime CreateDate { get; set; }
        public string Id { get; set; }
        public string PostId { get; set; }

        public string Description { get; set; }
        public string UserId { get; set; }
    }
}