using System;

namespace WebApplication4.Models.ApiModels.LikeApiModels
{
    public class CreateLikeApiModel
    {
        public string PostId { get; set; }
        public string UserId { get; set; }
    }
}