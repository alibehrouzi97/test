using System;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Models.RepoResults.Like;

namespace WebApplication4.Models.ApiModels.LikeApiModels
{
    public class LikeGetSummaryApiModels
    {
        public LikeGetSummaryApiModels(CreateLikeRepoResult createLikeRepoResult)
        {
            Id = createLikeRepoResult.Id;
            UserId = createLikeRepoResult.UserId;
        }
        public string Id { get; set; }
        public string UserId { get; set; }
    }
}