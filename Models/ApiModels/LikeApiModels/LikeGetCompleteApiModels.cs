using System;
using WebApplication4.Models.RepoResults.Like;

namespace WebApplication4.Models.ApiModels.LikeApiModels
{
    public class LikeGetCompleteApiModels
    {
        public LikeGetCompleteApiModels(CreateLikeRepoResult createLikeRepoResult)
        {
            Id = createLikeRepoResult.Id;
            PostId = createLikeRepoResult.PostId;
            UserId = createLikeRepoResult.UserId;
        }
        
        public string Id { get; set; }
        public string PostId { get; set; }
        public string UserId { get; set; }
    }
}