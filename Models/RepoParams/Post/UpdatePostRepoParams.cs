using System;
using WebApplication4.Models.ApiModels.PostApiModels;

namespace WebApplication4.Models.RepoParams.Post
{
    public class UpdatePostRepoParams
    {
        public UpdatePostRepoParams(UpdatePostApiModel postModel)
        {
            
            Description = postModel.Description;
            Title = postModel.Title;
            UserId = postModel.UserId;
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}