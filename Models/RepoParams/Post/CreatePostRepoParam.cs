using System;
using WebApplication4.Models.ApiModels.PostApiModels;

namespace WebApplication4.Models.RepoParams.Post
{
    public class CreatePostRepoParam
    {
        public CreatePostRepoParam(CreatePostApiModel postModel)
        {
            UserId = postModel.UserId;
            Title = postModel.Title;
            Description = postModel.Description;
            CommentCount = postModel.CommentCount;
        }
        public int CommentCount { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}