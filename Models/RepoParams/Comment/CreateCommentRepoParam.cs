using System;
using WebApplication4.Models.ApiModels.CommentApiModels;

namespace WebApplication4.Models.RepoParams.Comment
{
    public class CreateCommentRepoParam
    {
        public CreateCommentRepoParam(CreateCommentApiModel createCommentApiModel)
        {
            PostId = createCommentApiModel.PostId.ToString();
            Description = createCommentApiModel.Description;
            UserId = createCommentApiModel.UserId.ToString();
        }
        
        public string PostId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}