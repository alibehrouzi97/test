using System;
using WebApplication4.Models.ApiModels.CommentApiModels;

namespace WebApplication4.Models.RepoParams.Comment
{
    public class UpdateCommentRepoParam
    {
        public UpdateCommentRepoParam(UpdateCommentApiModel updateCommentApiModel)
        {
            PostId = updateCommentApiModel.PostId.ToString();
            Description = updateCommentApiModel.Description;
            UserId = updateCommentApiModel.UserId.ToString();
        }
        public string PostId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}