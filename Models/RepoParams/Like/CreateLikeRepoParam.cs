using System;
using WebApplication4.Models.ApiModels.LikeApiModels;

namespace WebApplication4.Models.RepoParams.Like
{
    public class CreateLikeRepoParam
    {
        public CreateLikeRepoParam(CreateLikeApiModel createLikeApiModel)
        {
            PostId = createLikeApiModel.PostId.ToString();
            UserId = createLikeApiModel.UserId.ToString();
        }
        public string PostId { get; set; }
        public string UserId { get; set; }
    }
}