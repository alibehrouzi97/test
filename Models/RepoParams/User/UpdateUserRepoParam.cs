using System;
using WebApplication4.Models.ApiModels.UserApiModels;

namespace WebApplication4.Models.RepoParams.User
{
    public class UpdateUserRepoParam
    {
        public UpdateUserRepoParam(UpdateUserApiModels updateUserApiModels)
        {
           
            FirstName = updateUserApiModels.FirstName;
            LastName = updateUserApiModels.LastName;
            UserName = updateUserApiModels.UserName;
            Password = updateUserApiModels.Password;
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}