using WebApplication4.Models.ApiModels.UserApiModels;

namespace WebApplication4.Models.RepoParams.User
{
    public class CreateUserRepoParam
    {
        public CreateUserRepoParam(CreateUserApiModels createUserApiModels)
        {
            FirstName = createUserApiModels.FirstName;
            LastName = createUserApiModels.LastName;
            UserName = createUserApiModels.UserName;
            Password = createUserApiModels.Password;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}