using System;
using WebApplication4.Models.ApiModels.PostApiModels;
using WebApplication4.Models.ApiModels.UserApiModels;
using WebApplication4.Models.RepoResults.User;

namespace WebApplication4.Models.Entities.User
{
    public class UserObject
    {
        private readonly CreateUserRepoResult _createUserRepoResult;


        public UserObject(CreateUserRepoResult createUserRepoResult)
        {
            _createUserRepoResult = createUserRepoResult;
            
        }

        public UserGetCompleteApiModels GetComplete()
        {
            if (_createUserRepoResult == null)
            {
                throw new Exception("User Repo Result is null");

            }
            
            return new UserGetCompleteApiModels(_createUserRepoResult);
        }
        
        public UserGetSummaryApiModels GetSummary()
        {
            if (_createUserRepoResult == null)
            {
                throw new Exception("User Repo Result is null");

            }
            
            return new UserGetSummaryApiModels(_createUserRepoResult);
        }
    }
}