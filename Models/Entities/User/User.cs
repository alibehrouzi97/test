using System;
using System.Collections.Generic;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.User;

namespace WebApplication4.Models.Entities.User
{
    public class User
    {
        
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public List<Post> Posts { get; set; }
    }
}