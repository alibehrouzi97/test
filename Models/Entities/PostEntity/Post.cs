using System;
using System.Collections.Generic;
using WebApplication4.Models.Entities.CommentEntity;
using WebApplication4.Models.Entities.LikeEntity;
using WebApplication4.Models.RepoParams.Post;

namespace WebApplication4.Models.Entities.PostEntity
{
    public class Post
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public int LikeCount  { get; set; }
        public int CommentCount { get; set; }
        public Guid UserId { get; set; }
        
        public List<Like> Likes { get; set; }
        public List<Comment> Comments { get; set; }
        public User.User User { get; set; }
        
    }
}