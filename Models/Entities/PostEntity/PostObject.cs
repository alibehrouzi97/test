using System;
using WebApplication4.Models.ApiModels.PostApiModels;
using WebApplication4.Models.RepoResults.Post;

namespace WebApplication4.Models.Entities.PostEntity
{
    public class PostObject
    {
        private readonly CreatePostRepoResult _createPostRepoResult;

        public PostObject(CreatePostRepoResult CreatePostRepoResult)
        {
            _createPostRepoResult = CreatePostRepoResult;
        }


        public PostGetCompleteApiModels GetComplete()
        {
            if (_createPostRepoResult == null)
            {
                //return null;
                throw new Exception("Repo result null");
            }
            return new PostGetCompleteApiModels(_createPostRepoResult);
        }

        public PostGetSummaryApiModels GetSummary()
        {
            if (_createPostRepoResult == null)
            {
                //return null;
                throw new Exception("Repo result null");
            }

            return new PostGetSummaryApiModels(_createPostRepoResult);
        }
    }
}