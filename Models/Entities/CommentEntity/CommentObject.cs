using System;
using WebApplication4.Models.ApiModels.CommentApiModels;
using WebApplication4.Models.RepoResults.Comment;

namespace WebApplication4.Models.Entities.CommentEntity
{
    public class CommentObject
    {
        private readonly CreateCommentRepoResult _createCommentRepoResult;

        public CommentObject(CreateCommentRepoResult commentRepoResult)
        {
            _createCommentRepoResult = commentRepoResult;
        }

        public CommentGetCompleteApiModels GetComplete()
        {
            if (_createCommentRepoResult == null)
            {
                //return null;
                throw new Exception("CreateCommentRepo result null,in CommentObject GetComplete");
            }
            return new CommentGetCompleteApiModels(_createCommentRepoResult);
        }

        public CommentGetSummaryApiModels GetSummary()
        {
            if (_createCommentRepoResult==null)
            {
                throw new Exception("CreateCommentRepo result null,in CommentObject GetComplete");
            }
            return new CommentGetSummaryApiModels(_createCommentRepoResult);
        }
    }
}