using System;
using System.Runtime.InteropServices.ComTypes;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.Comment;

namespace WebApplication4.Models.Entities.CommentEntity
{
    public class Comment
    {
        public DateTime CreateDate { get; set; }
        public Guid Id { get; set; }
        public Guid PostId { get; set; }
        public Post Post { get; set; }
        public string Description { get; set; }
        public Guid UserId { get; set; }
    }
}