using System;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoParams.Like;

namespace WebApplication4.Models.Entities.LikeEntity
{
    public class Like
    {
        public Guid Id { get; set; }
        public Guid PostId { get; set; }
        public Post Post { get; set; }
        public Guid UserId { get; set; }
        public User.User User { get; set; }
    }
}