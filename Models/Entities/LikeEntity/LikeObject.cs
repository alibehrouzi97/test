using System;
using WebApplication4.Models.ApiModels.LikeApiModels;
using WebApplication4.Models.ApiModels.PostApiModels;
using WebApplication4.Models.ApiModels.UserApiModels;
using WebApplication4.Models.Entities.PostEntity;
using WebApplication4.Models.RepoResults.Like;

namespace WebApplication4.Models.Entities.LikeEntity
{
    public class LikeObject
    {
        private readonly CreateLikeRepoResult _createLikeRepoResult;

        public LikeObject(CreateLikeRepoResult createLikeRepoResult)
        {
            _createLikeRepoResult = createLikeRepoResult;
        }
        public LikeGetCompleteApiModels GetComplete()
        {
            if (_createLikeRepoResult == null)
            {
                //return null;
                throw new Exception("CreateLikeRepo result null,in LikeObject GetComplete");
            }
            return new LikeGetCompleteApiModels(_createLikeRepoResult);
        }

        public LikeGetSummaryApiModels GetSummary()
        {
            if (_createLikeRepoResult == null)
            {
                //return null;
                throw new Exception("CreateLikeRepo result null,in LikeObject GetSummry");
            }

            return new LikeGetSummaryApiModels(_createLikeRepoResult);
        }
    }
}