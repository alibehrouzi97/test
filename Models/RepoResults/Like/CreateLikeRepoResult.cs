using System;

namespace WebApplication4.Models.RepoResults.Like
{
    public class CreateLikeRepoResult
    {

        public CreateLikeRepoResult(Entities.LikeEntity.Like like)
        {
            Id = like.Id.ToString();
            PostId = like.PostId.ToString();
            UserId = like.UserId.ToString();
        }
        public string Id { get; set; }
        public string PostId { get; set; }
        public string UserId { get; set; }
    }
}