using System;
using WebApplication4.Models.RepoParams.Post;

namespace WebApplication4.Models.RepoResults.Post
{
    public class CreatePostRepoResult
    {

        public CreatePostRepoResult(Entities.PostEntity.Post post)
        {
            Id = post.Id.ToString();
            Title = post.Title;
            Description = post.Description;
            CreateDate = post.CreateDate;
            EditDate = post.EditDate;
            LikeCount = post.LikeCount;
            UserId = post.UserId.ToString();
            CommentCount = post.CommentCount;

        }
        public int CommentCount { get; set; }
        public string UserId { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public int LikeCount { get; set; }        
    }
}