using System;

namespace WebApplication4.Models.RepoResults.Post
{
    public class ReadPostRepoResult
    {
                public Guid Id { get; set; }
                public string Title { get; set; }
                public string Description { get; set; }
                public DateTime CreateDate { get; set; }
                public DateTime? EditDate { get; set; }
                public int LikeCount { get; set; }        
                public Guid UserId { get; set; }
    }
}