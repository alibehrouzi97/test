using System;

namespace WebApplication4.Models.RepoResults.Comment
{
    public class CreateCommentRepoResult
    {
        public CreateCommentRepoResult(Entities.CommentEntity.Comment comment)
        {
            Id = comment.Id.ToString();
            PostId = comment.PostId.ToString();
            Description = comment.Description;
            UserId = comment.UserId.ToString();
            CreateDate = comment.CreateDate;
        }
        public DateTime CreateDate { get; set; }
        public string Id { get; set; }
        public string PostId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}