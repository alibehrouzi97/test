using System;
using WebApplication4.Models.RepoParams.Post;

namespace WebApplication4.Models.RepoResults.User
{
    public class CreateUserRepoResult
    {
        public CreateUserRepoResult(Entities.User.User user)
        {
            Id = user.Id.ToString();
            FirstName = user.FirstName;
            LastName = user.LastName;
            UserName = user.UserName;
            Password = user.Password;
        }
        
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}